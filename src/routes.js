import React from 'react';
import { Route, IndexRoute } from 'react-router';
import * as pageConstants from './constants/pageConstants';
import EnsureLoggedInContainer from './containers/EnsureLoggedInContainer';
import Layout from './components/layout/Layout';
import File from './components/content/file/File';
import Folder from './components/content/folder/Folder';
import History from './components/content/history/History';
import Favorite from './components/content/favorite/Favorite';
import Profile from './components/content/profile/Profile';
import Login from './components/login/Login';
import ForgotPass from './components/forgotpass/ForgotPass';

const reloadPage = (nextState, replace, callback) => {
  window.location = '/';
};

export default (
        <Route component={EnsureLoggedInContainer}>

                <Route path = {pageConstants.LOGIN} component={Login} />
                <Route path = {pageConstants.FORGOT_PASS} component={ForgotPass} />

                <Route path = {pageConstants.ROOT} component={Layout}>
                        <IndexRoute component={File}/>
                        <Route path = {pageConstants.FOLDER} component={Folder} />
                        <Route path = {pageConstants.HISTORY} component={History} />
                        <Route path = {pageConstants.FAVORITE} component={Favorite} />
                        <Route path = {pageConstants.PROFILE} component={Profile} />

                        {/* <Route path = {pageConstants.ERROR} component={ServerError} />
                        <Route path = {pageConstants.ERROR_204} component={Error204} />
                        <Route path = {pageConstants.ERROR_404} component={Error404} />
                        <Route path = {pageConstants.ERROR_500} component={Error500} />

                        <Route path = {pageConstants.LOGIN} component={Login} />
                        <IndexRoute component={SearchTheftFile}/>
                        <Route path = {pageConstants.SG_DETAIL + '/:fileNumber'} component={ServiceGuaranteeDetail} />
                        
                        <Route path = {pageConstants.SG_FILES} component={SearchLayout} >
                                <IndexRoute component={CustomerAwaitingPaymentFiles}/>
                                <Route path={pageConstants.SG_FILES_PAYMENT_AWAIT} component={CustomerAwaitingPaymentFiles}/> 
                                <Route path={pageConstants.SG_ALL_DECLINED_FILES} component={SGAllDeclinedFiles}/> 
                                <Route path={pageConstants.SG_ALL_DECLINED_FILES_EUSA} component={SGAllDeclinedFilesEUSA}/> 
                        </Route>
                        <Route path = {pageConstants.SETTING} component={Setting} />
                        <Route path = {pageConstants.HELP} component={Help} /> */}

                        
                </Route>
        </Route>
);
