import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';

let defaultState = {
  type:'',
  index: 1
};

export const updateMenuIndex = (index) => (   Object.assign({}, defaultState, { type: types.UPDATE_MENU_INDEX, loading:true, index})    );
export function updateMenuIndexSuccess(res) {
    return {
      loading: false,
      type: types.UPDATE_MENU_INDEX_SUCCESS,
      index: res
    };
}

export function getState() {
    return defaultState;
}
