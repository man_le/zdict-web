
// Left Menu Item
export const UPDATE_MENU_INDEX='UPDATE_MENU_INDEX';
export const UPDATE_MENU_INDEX_SUCCESS='UPDATE_MENU_INDEX_SUCCESS';

export const CHECK_LOGIN = 'CHECK_LOGIN';
export const CHECK_LOGIN_SUCCESS = 'CHECK_LOGIN_SUCCESS';
export const CHECK_LOGIN_FAIL = 'CHECK_LOGIN_FAIL';
export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';