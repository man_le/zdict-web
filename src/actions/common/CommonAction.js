import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';

let defaultState = {
  type:'',
  loading: false,
  currencyCodes: null
};

// Sort Result
export const getCurrencyCodes = () => ({type: types.GET_CURRENCY_CODES, loading: true});
export function getCurrencyCodesSuccess(res) {
    return {
      type: types.GET_CURRENCY_CODES_SUCCESS,
      loading: false,
      currencyCodes: res?res.data:null
    };
}

// Sort Result
export const insertPrintLog = (url, callback) => ({type: types.INSERT_PRINT_LOG, loading: true, url, callback});
export function insertPrintLogSuccess() {
    return {
      type: types.INSERT_PRINT_LOG_SUCCESS,
      loading: false
    };
}

export const cloneSummaryFileForEditing = (cloneName) => (   Object.assign({}, defaultState, { type: types.CLONE_SUMMARY_FILE, loading: true, cloneName})    );

export const retryCall = (type) => ({type: type});

export function getState() {
    return defaultState;
}
