import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';

let defaultState = {
  type:'',
  loading: false,
  searchCriteria: {
    value: '',
    sortCol: '',
    isAsc: true
  }
};

// Sort Result
export function sort(column, isAsc) {
    return {
      type: types.SORT,
      searchCriteria: {
          type: types.SORT,
          sortCol: column || '',
          isAsc: isAsc
      }
    };
}

export function getState() {
    return defaultState;
}
