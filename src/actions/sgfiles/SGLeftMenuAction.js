import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import * as R from '../../resources/R';
import { browserHistory } from 'react-router';

let defaultState = {
  type: '',
  index:1,
  url:'/',
  title: R.TITLE_THEFT_FILES
};

function switchItemMenuSuccess(index, url, title) {
    return {
      type: types.SG_SWITCH_LEFT_MENU_ITEM,
      index: index,
      url: url,
      title: title
    };
}
export function sgSwitchItemMenu(index, url, title) {
  return function (dispatch){
    dispatch(switchItemMenuSuccess(index, url, title));
  };
}

export function getState() {
    return defaultState;
}
