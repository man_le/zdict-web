import * as types from './../types';
import { browserHistory } from 'react-router';

let defaultState = {
};

export const sgSearchAllRequest = () => (Object.assign({}, defaultState,
    { type: types.SEARCH_ALL_SG_REQUEST}));
    
export const sgSearchAllSuccess = (data) => {
    return {
        type: types.SEARCH_ALL_SG_SUCCESS,
        ALL_SG: {payload: data }
        
    };
}

export const sgSearchCustAwaitPaymentRequest = () => (Object.assign({}, defaultState,
    { type: types.SEARCH_ALL_CUST_AWAIT_PAYMENT_REQUEST}));
    
export const sgSearchCustAwaitPaymentSuccess = (data) => {
    return {
        type: types.SEARCH_ALL_CUST_AWAIT_PAYMENT_SUCCESS,
        CUST_AWAIT_PAYMENT: {payload: data}
    };
}

export const sgSearchDeclinedList = () => (Object.assign({}, defaultState,
    { type: types.SEARCH_DECLINED_LIST_REQUEST}));
    
export const sgSearchDeclinedListSuccess = (data) => {
    return {
        type: types.SEARCH_DECLINED_LIST_REQUEST_SUCCESS,
        DECLINED_LIST: {payload: data}
    };
}

export const getState = () => defaultState;