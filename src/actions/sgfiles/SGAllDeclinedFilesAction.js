import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';

let defaultState = {
  type:'',
  loading: false,
  data:null
};

export const getSGDeclined = (criteria) => (   Object.assign({}, defaultState, { type: types.GET_SG_DECLINED, loading: true, criteria})    );
export function getSGDeclinedSuccess(res) {
    return Object.assign({}, defaultState, {
      type: types.GET_SG_DECLINED_SUCCESS,
      loading: false,
      data: res
    });
}

export const getSGDeclined4Eusa = (criteria) => (   Object.assign({}, defaultState, { type: types.GET_SG_DECLINED_EUSA, loading: true, criteria})    );
export function getSGDeclined4EusaSuccess(res) {
    return Object.assign({}, defaultState, {
      type: types.GET_SG_DECLINED_EUSA_SUCCESS,
      loading: false,
      data: res
    });
}

export function getState() {
    return defaultState;
}
