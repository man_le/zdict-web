import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';

let defaultState = {
  type:'',
  criteria:null
};

export const getCriteria = () => (   Object.assign({}, defaultState, { type: types.GET_CRITERIA})    );
export function getCriteriaSuccess(res) {
    return Object.assign({}, defaultState, {
      type: types.GET_CRITERIA_SUCCESS,
      criteria: res.data
    });
}

export function getState() {
    return defaultState;
}
