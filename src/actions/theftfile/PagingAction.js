import * as types from './../types';
import { browserHistory } from 'react-router';

let defaultState = {
  type:'',
  currentPage: 1,
  dataOnCurrentPage: [{
            'computraceFile': '',
            'customerId': '',
            'esn': '',
            'count': '',
            'reportedSerialNumber': '',
            'computerRecovered': '',
            'ownerName': '',
            'active': '',
            'cold': '',
            'followUp': '',
            'awaitingCustomer': '',
            'stolenAlert': '',
            'stolenFlag': ''
  }]
};

// Switch page
export function switchPage(data, currentPage, maxRow) {
  return function (dispatch){
    dispatch(switchPageSuccess(data, currentPage, maxRow));
  };
}
export function switchPageSuccess(data, currentPage, maxRow) {

    let fromItem = (currentPage-1)*maxRow;
    let currentData = data.slice(fromItem, fromItem+maxRow+1);
    return {
      type: types.PAGING,
      currentPage: currentPage,
      dataOnCurrentPage: currentData
    };
}

// Reset page
export function resetPage() {
  return function (dispatch){
    dispatch(resetPageSuccess());
  };
}
export function resetPageSuccess() {
    return Object.assign({}, defaultState, {
        type: types.RESET_CURRENT_PAGE,
        currentPage: 1
      }
    );
}

export function getState() {
    return defaultState;
}
