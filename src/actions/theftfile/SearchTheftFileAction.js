import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';

let defaultState = {
  type:'',
  loading: false,
  searchCriteria: {
    value: '',
    sortCol: '',
    isAsc: true
  },
  theftFiles: [{
            'computraceFile': '',
            'customerId': '',
            'esn': '',
            'count': '',
            'reportedSerialNumber': '',
            'computerRecovered': '',
            'ownerName': '',
            'active': '',
            'cold': '',
            'followUp': '',
            'awaitingCustomer': '',
            'stolenAlert': '',
            'stolenFlag': ''
  }],
};

// Sort Result
function sortSuccess(theftFiles, column, isAsc) {
    [].sort.call(theftFiles, (a, b) => {
        let result = a[column] - b[column];
        if(lodash.isNull(a[column])){
            return 1;
        }else if(lodash.isNull(b[column])){
            return -1;
        }else if(a[column] == b[column]){
            return 0;
        }else if(isNaN(result)) {
            return (isAsc) ?
                a[column].toLowerCase().localeCompare(b[column].toLowerCase())
                :
                b[column].toLowerCase().localeCompare(a[column].toLowerCase())
        }else{
            return (isAsc) ? result : -result;
        }
    });
    return {
      type: types.SORT_THEFT_FILE_RESULT,
      searchCriteria: {
          sortCol: column || '',
          isAsc: isAsc
      },
      theftFiles: theftFiles
    };
}
export function sort(theftFiles, column, isAsc) {
  return function (dispatch){
    dispatch(sortSuccess(theftFiles, column, isAsc));
  };
}

// Search Theft File
export const search = fileNumber => (   Object.assign({}, defaultState, { type: types.SEARCH_THEFT_FILE, loading: true,fileNumber })    );
export function searchTheftFileSuccess(value, res) {

    let response = res.response.data;
    if(!response) response = [{'computraceFile': '' , 'message':'not found'}];
    if(response && Object.keys(response).length == 1 && response[0].computraceFile){
        browserHistory.push(pageConstants.SG_DETAIL+'/'+value);
    }
    return Object.assign({}, defaultState, {
      loading: false,
      type: types.SEARCH_THEFT_FILE_SUCCESS,
      searchCriteria: {
          value: value || ''
      },
      theftFiles: response
    });
}

// Reset stage
function resetStageSuccess() {
    return Object.assign({}, defaultState, {
      type: types.RESET_THEFTFILE_STAGE,
    });
}
export function resetSearchTheftFileStage() {
  return function (dispatch){
    dispatch(resetStageSuccess());
  };
}

export function getState() {
    return defaultState;
}
