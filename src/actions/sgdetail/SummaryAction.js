import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';

let defaultState = {
  type:'',
  loading: false,
  summary:{
        file:null,
        customers:null,
        addresses:null,
        licenses:null
  }
};

export const getSummary = (fileNumber) => (   Object.assign({}, defaultState, { type: types.GET_SUMMARY, loading: true, fileNumber})    );
export function getSummarySuccess(res) {
    return Object.assign({}, defaultState, {
      loading: false,
      type: types.GET_SUMMARY_SUCCESS,
      summary: res
    });
}

export const saveCustomerContact = (contactId, fileNumber) => ({type: types.SAVE_CUSTOMER_CONTACT, savingSummaryContact: true, contactId, fileNumber });
export function saveCustomerContactSuccess(file) {
    return {
      savingSummaryContact: false,
      type: types.SAVE_CUSTOMER_CONTACT_SUCCESS,
      file
    };
}

export const getSummaryFile = (fileNumber) => (   Object.assign({}, defaultState, { type: types.GET_SUMMARY_FILE, fileNumber})    );
export function getSummaryFileSuccess(res) {
    return Object.assign({}, defaultState, {
      type: types.GET_SUMMARY_FILE_SUCCESS,
      file: res.data
    });
}

export const getPrepaidBalance = (prepaidBalanceUrl) => (   Object.assign({}, 
        { type: types.GET_PREPAID_BALANCE, loading: true, prepaidBalanceUrl})    );

export const getPrepaidBalanceSuccess = (res) => {
    return Object.assign({}, {
      loading: false,
      type: types.GET_PREPAID_BALANCE_SUCCESS,
      balances: res
    });
}

export function getState() {
    return defaultState;
}
