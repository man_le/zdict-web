import * as types from './../types';
import { browserHistory } from 'react-router';
import lodash from 'lodash';

let defaultState = {
  type:'',
  loading: false,
  eligibility:[],
  autocheck:{},
  check:{},
  currentCriterias:[],
  emailDeclinedReview: null
};

export const emailDeclinedReview = (url, fileNumber, criteria) => (   Object.assign({}, defaultState, { type: types.EMAIL_DECLINED_REVIEW, loading: true, url, fileNumber, criteria})    );
export function emailDeclinedReviewSuccess(res) {
    res = res.response.data;
    return {
      loading: false,
      type: types.GET_ELIGIBILITY_SUCCESS,
      emailDeclinedReview: res
    };
}

export function getState() {
    return defaultState;
}
