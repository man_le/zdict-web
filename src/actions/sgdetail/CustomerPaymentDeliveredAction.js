import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';
import * as errorTypes from './../../constants/errorTypes';

let defaultState = {
  type:'',
  loading: false,
  paymentInfo : null,
  alternateAddress: null
};

export const getPaymentInfo = (url) => (   Object.assign({}, defaultState, { type: types.GET_PAYMENT_INFO, loading: true, url})    );
export function getPaymentInfoSuccess(res) {
    return {
      loading: false,
      type: types.GET_PAYMENT_INFO_SUCCESS,
      data: res.data
    };
}

export const getAlternateAddress = (url) => (   Object.assign({}, defaultState, { type: types.GET_ALTERNATE_ADDRESS, loading: true, url})    );
export function getAlternateAddressSuccess(res) {
    return {
      loading: false,
      type: types.GET_ALTERNATE_ADDRESS_SUCCESS,
      data: res?res.data:null
    };
}

export const getAddressByAccountIdOrEsn = () => (   Object.assign({}, defaultState, { type: types.GET_ADDRESS_BY_ACC_ESN, loading: true})    );
export function getAddressByAccountIdOrEsnSuccess(res) {
    return {
      loading: false,
      type: types.GET_ADDRESS_BY_ACC_ESN_SUCCESS,
      data: res?res.data:null
    };
}

export function getState() {
    return defaultState;
}
