import * as types from './../types';

export const getAllSentItems = (itemsSentUrl) => (   Object.assign({}, 
        { type: types.ALL_SENT_ITEMS, loading: true, itemsSentUrl})    );

export const getAllSentItemsSuccess = (res) => {
  console.log(res);
    return Object.assign({}, {
      loading: false,
      type: types.ALL_SENT_ITEMS_SUCCESS,
      sentItems: res
    });
}