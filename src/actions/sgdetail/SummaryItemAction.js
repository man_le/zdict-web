import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';

let defaultState = {
  type:'',
  file:null
};

export const updatePaymentAddress = (apiLinks, fileNumber, field, value, addressId) => (
                                                                                        Object.assign({}, 
                                                                                        defaultState, 
                                                                                        { 
                                                                                          type: types.UPDATE_SUMMARY_ADDRESS, 
                                                                                          savingPaymentAddress: true,
                                                                                          apiLinks, 
                                                                                          fileNumber, 
                                                                                          field, 
                                                                                          value, 
                                                                                          addressId
                                                                                        }));
export function updatePaymentAddressSuccess(res, field) {
    return Object.assign({}, defaultState, {
      savingPaymentAddress: false,
      field: field,
      type: types.UPDATE_SUMMARY_ADDRESS_SUCCESS,
      file: res.data
    });
}

export const updatePaymentAddressFull = (apiLinks, jsonObj) => (   Object.assign({}, defaultState, { type: types.UPDATE_SUMMARY_ADDRESS_FULL, apiLinks, jsonObj})    );
export function updatePaymentAddressFullSuccess(res) {
    return Object.assign({}, defaultState, {
      type: types.UPDATE_SUMMARY_ADDRESS_FULL_SUCCESS,
      file: res.data
    });
}

export function getState() {
    return defaultState;
}
