import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';

let defaultState = {
  type:'',
  notes:null,
  noteIndex:null,
  count:0
};

export const getNotes = (url) => (   Object.assign({}, defaultState, { type: types.GET_NOTES, loading: true, url})    );
export function getNotesSuccess(res) {

    let data = res.data;
    if(lodash.isNull(data)) data = [];
    return Object.assign({}, defaultState, {
      type: types.GET_NOTES_SUCCESS,
      notes: data,
      noteIndex: res.count-1,
      count: res.count
    });
}

export function switchNote(index) {
    return {
      type: types.SWITCH_NOTE,
      noteIndex: index
    };
}

export const createNote = (apiLinks, fileNumber, note) => (   Object.assign({}, defaultState, { type: types.CREATE_NOTES, apiLinks, fileNumber, note})    );
export function createNoteSuccess(res) {

    let data = res.data;
    if(lodash.isNull(data)) data = [];

    return Object.assign({}, defaultState, {
      type: types.CREATE_NOTES_SUCCESS,
      notes: data,
      noteIndex: res.count-1,
      count: res.count
    });
}

export function getState() {
    return defaultState;
}
