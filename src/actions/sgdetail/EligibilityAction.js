import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';
import * as errorTypes from './../../constants/errorTypes';

let defaultState = {
  type:'',
  loading: false,
  disabledCheck : false,
  eligibility:[],
  autocheck:{},
  check:{},
  currentCriterias:[],
  emailDeclinedReview: null,
  sendEmailDeclined: null
};

export function removeCurrentCriteria(currentCriterias, criteriaSelected) {
    let result = currentCriterias;
    for(let i=0; i<result.length;i++){
      if(result[i]==criteriaSelected){
        result.splice(i,1);
        break;
      }
    }
    return {type: types.REMOVE_CURRENT_CRITERIA, currentCriterias: result};
}
export function addCurrentCriteria(currentCriterias, criteriaSelected) {
    let result = currentCriterias;
    if(result.indexOf(criteriaSelected)==-1 && !lodash.isUndefined(criteriaSelected) && !lodash.isNull(criteriaSelected)) result.push(criteriaSelected)
    return {type: types.ADD_CURRENT_CRITERIA, currentCriterias: result};
}

export const getEligibility = fileNumber => (   Object.assign({}, defaultState, { type: types.GET_ELIGIBILITY, loading: true, fileNumber})    );
export function getEligibilitySuccess(res, trmsRgCriteriaTypeID) {
    return Object.assign({}, defaultState, {
      loading: false,
      type: types.GET_ELIGIBILITY_SUCCESS,
      eligibility: res.eligibility,
      autocheck: res.autocheck,
      check: res.check,
      trmsRgCriteriaTypeID : trmsRgCriteriaTypeID
    });
}

export function getChecksSuccess(res, checkTypeId) {
    return {
      loading: false,
      type: types.GET_CHECKS_SUCCESS,
      check: res,
      checkTypeId: checkTypeId
    };
}

export const getCriteriaOnly = () => (   Object.assign({}, defaultState, { type: types.GET_CRITERIA_ONLY, loading: true})    );
export function getCriteriaOnlySuccess(res) {
    return {
      loading: false,
      type: types.GET_CRITERIA_ONLY_SUCCESS,
      eligibility: res.data
    };
}

export const getAutoCheckOnly = () => (   Object.assign({}, defaultState, { type: types.GET_AUTOCHECK_ONLY, loading: true})    );
export function getAutoCheckOnlySuccess(res) {
    return {
      loading: false,
      type: types.GET_AUTOCHECK_ONLY_SUCCESS,
      autocheck: res.data
    };
}

export const getCheckOnly = () => (   Object.assign({}, defaultState, { type: types.GET_CHECK_ONLY, loading: true})    );
export function getCheckOnlySuccess(res) {
    return {
      loading: false,
      type: types.GET_CHECK_ONLY_SUCCESS,
      check: res.data
    };
}

export const emailDeclinedReview = (url, fileNumber, criteria) => (   Object.assign({}, defaultState, { type: types.EMAIL_DECLINED_REVIEW, loading: true, url, fileNumber, criteria})    );
export function emailDeclinedReviewSuccess(res) {
    res = res.response;
    if(!lodash.isNull(res) && !lodash.isEmpty(res))
        res = res.data;
    else 
        res = errorTypes.ERROR_NO_CONTACT;
    return {
      loading: false,
      type: types.EMAIL_DECLINED_REVIEW_SUCCESS,
      emailDeclinedReview: res
    };
}

export const sendEmailDeclined = (url, fileNumber, criteria) => (   Object.assign({}, defaultState, { type: types.SEND_EMAIL_DECLINED, loading: true, url, fileNumber, criteria})    );
export function sendEmailDeclinedSuccess(res) {
    res = res.response.data;
    return {
      loading: false,
      type: types.SEND_EMAIL_DECLINED_SUCCESS,
      sendEmailDeclined: res,
      currentCriterias:[]
    };
}

export const overrideCriteria = (currentCriterias, fileNumber, trmsRgCriteriaTypeID, reason) => (   Object.assign({}, defaultState, { type: types.OVERRIDE_CRITERIA, loading: true, currentCriterias, fileNumber, trmsRgCriteriaTypeID, reason})    );
export const removeOverrideCriteria = (currentCriterias, fileNumber, trmsRgCriteriaTypeID) => (   Object.assign({}, defaultState, { type: types.REMOVE_OVERRIDE_CRITERIA, loading: true, currentCriterias, fileNumber, trmsRgCriteriaTypeID})    );

export const getEligibilityCheck = (url, fileNumber) => ({ type: types.GET_ELIGIBILITY_CHECK, url, fileNumber});
export function getEligibilityCheckSuccess(res) {
    res = res.response.data;
    return {
      type: types.GET_ELIGIBILITY_CHECK_SUCCESS,
      check: res,
    };
}


export function getState() {
    return defaultState;
}
