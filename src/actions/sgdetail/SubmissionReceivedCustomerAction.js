import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';
import * as errorTypes from './../../constants/errorTypes';

let defaultState = {
  type:'',
  loading: false,
  deemedProofOfPurchaseAmount:null,
  prepaidBalance:null,
  summaryFileClone:null,
  maxPaymentProfile:null
};

export const saveSubmissionDueDate = (saveSubmissionDueDate) => (   Object.assign({}, defaultState, { type: types.SAVE_SUBMISSION_DUE_DATE, loading: true, saveSubmissionDueDate})    );
export function saveSubmissionDueDateSuccess(res) {
    return {
      loading: false,
      type: types.SAVE_SUBMISSION_DUE_DATE_SUCCESS,
    };
}

export const getDeemedProofOfPurchaseAmount = () => (   Object.assign({}, defaultState, { type: types.GET_PROOF_OF_PURCHASE_AMOUNT, loading: true})    );
export function getDeemedProofOfPurchaseAmountSuccess(res) {
    return {
      loading: false,
      type: types.GET_PROOF_OF_PURCHASE_AMOUNT_SUCCESS,
      deemedProofOfPurchaseAmount: res.data
    };
}

export const getMaxPaymentProfile = () => (   Object.assign({}, defaultState, { type: types.GET_MAX_PAYMENT_PROFILE, loading: true})    );
export function getMaxPaymentProfileSuccess(res) {
    return {
      loading: false,
      type: types.GET_MAX_PAYMENT_PROFILE_SUCCESS,
      maxPaymentProfile: res.data
    };
}

export const getPrepaidBalance = () => (   Object.assign({}, defaultState, { type: types.GET_PREPAID_BALANCE_SUB, loading: true})    );
export function getPrepaidBalanceSuccess(res) {
    return {
      loading: false,
      type: types.GET_PREPAID_BALANCE_SUB_SUCCESS,
      prepaidBalance: res.data
    };
}

export const savePaymentAmount = (data) => (   Object.assign({}, defaultState, { type: types.SAVE_PAYMENT_AMOUNT, loading: true, data})    );
export function savePaymentAmountSuccess(res) {
    return {
      loading: false,
      type: types.SAVE_PAYMENT_AMOUNT_SUCCESS
    };
}

export function getState() {
    return defaultState;
}