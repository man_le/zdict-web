import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';
import * as errorTypes from './../../constants/errorTypes';

let defaultState = {
  type:'',
  loading: false
};

export const updateCurrentBalance = (data) => (   Object.assign({}, defaultState, { type: types.UPDATE_CURRENT_BALANCE, data})    );
export function updateCurrentBalanceSuccess() {
    return {
      type: types.UPDATE_CURRENT_BALANCE_SUCCESS
    };
}

export const getPrepaidBalanceEvent = () => (   Object.assign({}, defaultState, { type: types.GET_PREPAID_BALANCE_EVENT, balancesEvent:null})    );
export function getPrepaidBalanceEventSuccess(res) {
    return {
      type: types.GET_PREPAID_BALANCE_EVENT_SUCCESS,
      balancesEvent: res
    };
}

export function getState() {
    return defaultState;
}