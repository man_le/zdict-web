import * as types from './../types';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';
import * as errorTypes from './../../constants/errorTypes';

let defaultState = {
  type:'',
  loading: false,
  data : null
};

export const emailSubmissionReview = (url) => (   Object.assign({}, defaultState, { type: types.EMAIL_SUBMISSION_REVIEW, loading: true, url})    );
export function emailSubmissionReviewSuccess(res) {
    if(!lodash.isNull(res) && !lodash.isEmpty(res))
        res = res.data;
    else 
        res = errorTypes.ERROR_NO_CONTACT;
    return {
      loading: false,
      type: types.EMAIL_SUBMISSION_REVIEW_SUCCESS,
      data: res
    };
}

export const sendEmailSubmission = (url) => (   Object.assign({}, defaultState, { type: types.SEND_EMAIL_SUBMISSION, loading: true, url})    );
export function sendEmailSubmissionSuccess(res) {
    res = res.response.data;
    return {
      loading: false,
      type: types.SEND_EMAIL_SUBMISSION_SUCCESS,
    };
}

export function getState() {
    return defaultState;
}
