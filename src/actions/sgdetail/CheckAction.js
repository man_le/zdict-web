import * as types from './../types';
import { browserHistory } from 'react-router';

let defaultState = {
  type:'',
  data:null
};

export const updateCheck = (apiLinks, fileNumber, checkTypeId, value) => (   Object.assign({}, defaultState, { type: types.UPDATE_CHECK, apiLinks, fileNumber, checkTypeId, value})    );

export function getState() {
    return defaultState;
}