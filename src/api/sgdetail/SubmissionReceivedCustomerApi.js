import ajax from 'superagent';
import * as urlConstants from './../../constants/urlConstants';
import async from 'async';
import { Observable } from 'rxjs/Observable'
import BaseApi from './../BaseApi'
import lodash from 'lodash';
import { ajax as rxajax } from 'rxjs/observable/dom/ajax';

export default class SubmissionReceivedCustomerApi extends BaseApi{

    static saveSubmissionDueDate(url, submissionDueDate) {
        return Observable.ajax.post(url, {submissionDueDate}, {'Content-Type': 'application/x-www-form-urlencoded'});
    }

    static getDeemedProofOfPurchaseAmount(url) {
        return rxajax.getJSON(url);
    }

    static getPrepaidBalance(url) {
        return rxajax.getJSON(url);
    }

    static getMaxPaymentProfile(url) {
        //rxajax.getJSON(url).subscribe(res => console.log("RR " + res), err => console.log("EE " + res));
        return rxajax.getJSON(url);
    }

    static savePaymentAmount(url, data) {
        return Observable.ajax.post(url, data, {'Content-Type': 'application/json'});
    }

}