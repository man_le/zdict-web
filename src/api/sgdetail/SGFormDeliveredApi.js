import ajax from 'superagent';
import * as urlConstants from './../../constants/urlConstants';
import async from 'async';
import { Observable } from 'rxjs/Observable'
import baseApi from './../BaseApi'
import lodash from 'lodash';
import { ajax as rxajax } from 'rxjs/observable/dom/ajax';

export default class SGFormDeliveredApi extends baseApi{

  static emailSubmissionReview(url){
    return rxajax.getJSON(url);
  }

  static emailSubmissionSend(url){
    return Observable.ajax.post(url, {'Content-Type': 'application/json'});
  }


}