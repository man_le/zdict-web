import ajax from 'superagent';
import * as urlConstants from './../../constants/urlConstants';
import async from 'async';
import { Observable } from 'rxjs/Observable'
import baseApi from './../BaseApi'
import lodash from 'lodash';
import { ajax as rxajax } from 'rxjs/observable/dom/ajax';

export default class EligibilityApi extends baseApi{

  static waterFallCall = Observable.bindCallback(async.waterfall);
  
  static getEligibility(fileNumber) {
    let urlEligibility = urlConstants.GET_ELIGIBILITY.replace(/\{0\}/g, `${fileNumber}`);
    let urlEligibilityAutoCheck = urlConstants.GET_ELIGIBILITY_AUTOCHECK.replace(/\{0\}/g, `${fileNumber}`);
    let urlEligibilityCheck = urlConstants.GET_ELIGIBILITY_CHECK.replace(/\{0\}/g, `${fileNumber}`);
    return this.waterFallCall([
          callback => {
                            async.parallel({
                              eligibility: callback => super.call(urlEligibility, callback),
                              autocheck: callback => super.call(urlEligibilityAutoCheck, callback),
                              check: callback => super.call(urlEligibilityCheck, callback)
                            }, (err, results) => callback(err, results));
                     }
      ]);
  }

  static getCriteriaOnly(url) {
      return rxajax.getJSON(url);
  }

  static getAutoCheckOnly(url){
     return rxajax.getJSON(url);
  }

  static getCheckOnly(url){
     return rxajax.getJSON(url);
  }

  static overrideCriteria(fileNumber, trmsRgCriteriaTypeID, reason){
    let urlOverride = urlConstants.POST_OVERRIDE;
    urlOverride = urlOverride.replace(/\{fileNumber\}/g, `${fileNumber}`);
    urlOverride = urlOverride.replace(/\{trmsRgCriteriaTypeID\}/g, `${trmsRgCriteriaTypeID}`);
    return Observable.ajax.post(urlOverride, {'reason':reason}, {'Content-Type': 'application/x-www-form-urlencoded'});
  }

  static removeOverrideCriteria(fileNumber, trmsRgCriteriaTypeID){
    let urlOverride = urlConstants.POST_REMOVE_OVERRIDE;
    urlOverride = urlOverride.replace(/\{fileNumber\}/g, `${fileNumber}`);
    urlOverride = urlOverride.replace(/\{trmsRgCriteriaTypeID\}/g, `${trmsRgCriteriaTypeID}`);
    return Observable.ajax.post(urlOverride);
  }

  static emailDeclined(url, fileNumber, criteria){
    return Observable.ajax.post(url, {fileNumber, criteria}, {'Content-Type': 'application/json'});
  }
}