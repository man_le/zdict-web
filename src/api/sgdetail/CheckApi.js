import ajax from 'superagent';
import * as urlConstants from './../../constants/urlConstants';
import async from 'async';
import { Observable } from 'rxjs/Observable'
import BaseApi from './../BaseApi'
import lodash from 'lodash';
import { ajax as rxajax } from 'rxjs/observable/dom/ajax';

export default class CheckApi extends BaseApi{

    static updateCheck(url, fileNumber, checkTypeId, value) {
        return Observable.ajax.post(url, {fileNumber, checkTypeId, value}, {'Content-Type': 'application/json'});
    }

}