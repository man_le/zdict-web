import ajax from 'superagent';
import * as urlConstants from './../../constants/urlConstants';
import async from 'async';
import { Observable } from 'rxjs/Observable'
import BaseApi from './../BaseApi'
import lodash from 'lodash';
import { ajax as rxajax } from 'rxjs/observable/dom/ajax';

export default class SummaryApi extends BaseApi{

    static waterFallCall = Observable.bindCallback(async.waterfall);

    static getSummaryNote(url) {
        return rxajax.getJSON(url);
    }

    static createSummaryNote(url, fileNumber, note) {
        return Observable.ajax.post(url, {fileNumber,note}, {'Content-Type': 'application/json'});
    }

    static getSummaryFileOnly(fileNumber) {
        return rxajax.getJSON(urlConstants.GET_THEFT_FILE_DETAIL + `/${fileNumber}`);
    }

    static updateSummaryPaymentAddress(url, fileNumber, field, value, addressId) {
        return Observable.ajax.post(url, {fileNumber, [field]: value, addressId}, {'Content-Type': 'application/json'});
    }

    static updateSummaryPaymentAddressFull(url, jsonObj) {
        return Observable.ajax.post(url, jsonObj, {'Content-Type': 'application/json'});
    }

    static saveCustomerContact(url, customerContactId) {
        return Observable.ajax.post(url,{customerContactId}, {'Content-Type': 'application/x-www-form-urlencoded'});
    }

    static getSummary(fileNumber) {
        return this.waterFallCall([
            callback => super.call(urlConstants.GET_THEFT_FILE_DETAIL + `/${fileNumber}`, callback, 'file', 'links'),
            (json, callback) => {

                if(lodash.isEmpty(json.file)){
                    callback(null, null);
                    return;
                }

                let data = json.file;
                let links = json.links;
                async.parallel({
                    apilinks: callback => callback(null, links),
                    licenses: callback => super.call(links.licenseUsageSummaryUrl, callback),
                    customers: callback => super.call(links.accountCustomerUrl, callback),
                    addresses: callback => {
                                            let url = links.paymentAddressByEsnUrl;
                                            if(data.productType === 'CORP'){
                                                url = links.paymentAddressByAccountUrl;
                                            }
                                            super.call(url, callback)
                                        }
                }, (err, results) => {
                    results.file = data;
                    callback(err, results)
                });
            }
        ]);
    }


    static getSentItems(url="") {
        return rxajax.getJSON(url);
    }

    static getPrepaidBalance(url="") {
        return rxajax.getJSON(url);
    }

    static updateCurrentBalance(url, data) {
        return Observable.ajax.post(url, data, {'Content-Type': 'application/json'});
    }
}