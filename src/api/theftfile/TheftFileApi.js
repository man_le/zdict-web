import ajax from 'superagent';
import * as urlConstants from './../../constants/urlConstants';
import async from 'async';
import { Observable } from 'rxjs/Observable'
import lodash from 'lodash';

class TheftFileApi{
  static search(fileNumber) {
    return Observable.ajax.get(urlConstants.SEARCH_THEFT_FILE_ALL + `?fileNumber=${fileNumber}`);
  }
}

export default TheftFileApi;