import ajax from 'superagent';
import { Observable } from 'rxjs/Observable'
import * as urlConstants from './../../constants/urlConstants';
import { ajax as rxajax } from 'rxjs/observable/dom/ajax';

class SgSearchApi{

  static all() {
    return rxajax.getJSON(urlConstants.ALL_SG_FILE_SEARCH);
  }

  static allCriteria() {
    return rxajax.getJSON(urlConstants.ALL_CRITERIA);
  }

  static sgDeclined(criteria) {
    return rxajax.getJSON(urlConstants.SG_DECLINED + criteria);
  }

  static sgEusaDeclined(criteria) {
    return rxajax.getJSON(urlConstants.SG_EUSA_DECLINED + criteria);
  }

  static customerAwaitingPayment() {
    return rxajax.getJSON(urlConstants.CUST_AWAIT_PAYMENT_SG_FILE_SEARCH);
  }
}
export default SgSearchApi;