import ajax from 'superagent';
import * as urlConstants from './../constants/urlConstants';
import async from 'async';
import { Observable } from 'rxjs/Observable'
import lodash from 'lodash';
import { ajax as rxajax } from 'rxjs/observable/dom/ajax';

class BaseApi{
  static call(url, callback, paramForBuildNewJson, links){
    ajax.get(url)
      .end((err, res) => {

        let data = null;
        if(res && !lodash.isNull(res.body)) data = res.body.data;

        if(lodash.isEmpty(res)){
          callback(null, null);
          return;
        } 

        if (err) {
          callback(err, null);
        }
        let buildJsonResult = {};
        if(paramForBuildNewJson){
          buildJsonResult[paramForBuildNewJson] = data;

          if(links){
            let obj = res.body;
            if(!lodash.isNull)delete obj.data;
            buildJsonResult[links] = obj;
          }
        }else{
          buildJsonResult = data;
        }

        callback(null, buildJsonResult);
    });
  }

  static getCurrencyCodes(){
      return rxajax.getJSON(urlConstants.GET_CURRENCY_CODES);
  }

  static callPost(url){
      return Observable.ajax.post(url, {}, {'Content-Type': 'application/x-www-form-urlencoded'});
  }

  static callGet(url){
    return rxajax.getJSON(url);
  }

}

export default BaseApi;