export const ROOT = '/';
export const LOGIN = '/login';
export const FORGOT_PASS = '/forgotpass';
export const ERROR = '/page/error';
export const ERROR_204 = '/page/204';
export const ERROR_404 = '/page/404';
export const ERROR_500 = '/page/500';

export const FOLDER = '/list';
export const HISTORY = '/history';
export const FAVORITE = '/favorite';
export const PROFILE = '/profile';
export const HELP = '/help';
