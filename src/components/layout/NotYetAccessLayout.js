import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';
import '../../assets/z/styles/styles.scss';
import * as R from '../../resources/R';

class NotYetAccessLayout extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        const {loginReducer} = this.props;
        if(loginReducer.userInfo) return null;
        else return <div style={{fontSize:15, minWidth:884}}>
                        <div style={{height:50}}>
                            menu bar
                        </div>
                        <div style={{width:'80%', margin:'0 auto', marginTop:30}}>
                            <div className='col-md-6 col-sm-6'>adf</div>
                            {this.props.children}
                        </div>

                    </div>
    }
}

const mapStateToProps = (state) => {
    return {
        loginReducer: state.loginReducer
    };
};

export default connect(
    mapStateToProps
)(NotYetAccessLayout);
