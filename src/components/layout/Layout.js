import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import '../../assets/z/styles/styles.scss';
import TopMenu from './../menu/TopMenu';
import LeftMenu from './../menu/LeftMenu';
import Content from './../content/Content';
import * as R from '../../resources/R';

class Layout extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        const {loginReducer} = this.props;
        if(loginReducer.userInfo) 
            return (
                <div>
                    <LeftMenu/>
                    <Content>{this.props.children}</Content>
                </div>
            )
        else return null;
    }
}

const mapStateToProps = (state) => {
    return {
        loginReducer : state.loginReducer
    };
};

export default connect(
    mapStateToProps
)(Layout);
