import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import '../../assets/z/styles/styles.scss';
import * as R from '../../resources/R';

class TopMenu extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        return <div style={{zIndex:2, position:'fixed', width:'100%'}}>
                   <div className='z_boxshadow' style={{color:'#A0A0A0', backgroundColor:'#fff', height:55, padding:16, paddingLeft:25}}>
                       <div style={{float:'left', marginTop:-8}}>
                           <button className='z_btn btn btn-link' style={{marginRight:20}}>
                                <span style={{color:'#A0A0A0', paddingTop:5, fontSize:17}} className='glyphicon glyphicon-option-vertical'></span>
                           </button>
                       </div>
                       <div style={{float:'left', backgroundColor:'blue', width:60, marginRight:20}}>Logo</div>

                       <div style={{marginLeft:70, float:'left', marginTop:-7, width:'50%'}}>
                                <div className="form-group">
                                    <input style={{paddingRight:30, float:'left', width:'100%'}} type="text" className="form-control" placeholder="Search"/>
                                    <div className='glyphicon glyphicon-search' style={{color:'#A0A0A0', float:'right', marginTop:-25, marginRight:8, fontSize:17}}></div>
                                </div>
                       </div>

                       <div style={{float:'right', marginTop:-8}}>
                           <button className='btn btn-link' style={{marginRight:5}}>
                                <span style={{color:'#A0A0A0', paddingTop:5, fontSize:17}} className='glyphicon glyphicon-shopping-cart'></span>
                           </button>
                            
                           <div className="btn-group" style={{marginRight:40}}>
                                <button type="button" className="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span style={{color:'#A0A0A0', paddingTop:5, fontSize:17}} className='glyphicon glyphicon-user'></span>
                                </button>
                                <ul className="dropdown-menu" style={{marginLeft:-70}}>
                                    <li><a href="#">{R.VIEW_ORDER}</a></li>
                                    <li><a href="#">{R.VIEW_ACCOUNT}</a></li>
                                    <li><a href="#">{R.PRODUCT_HISTORY}</a></li>
                                    <li><a href="#">{R.ADDRESS_BOOK}</a></li>
                                    <li role="separator" className="divider"></li>
                                    <li><a href="#">{R.LOGOUT}</a></li>
                                </ul>
                            </div>
                       </div>
                   </div>
                </div>
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

export default connect(
    mapStateToProps
)(TopMenu);
