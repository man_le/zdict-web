import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import * as pageConstants from './../../constants/pageConstants';
import '../../assets/z/styles/styles.scss';
import * as R from '../../resources/R';
import LeftMenuItem from './LeftMenuItem';

class LeftMenu extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        return <div className='z_leftMenu'>

                    <div style={{marginBottom:42}}>icon</div>
                    <LeftMenuItem url={pageConstants.ROOT} index={1} icon='pe-7s-search'/>
                    <LeftMenuItem url={pageConstants.HISTORY} index={2} icon='pe-7s-stopwatch'/>
                    <LeftMenuItem url={pageConstants.FAVORITE} index={3} icon='pe-7s-like'/>

                    <LeftMenuItem url={pageConstants.PROFILE} index={4}  icon='pe-7s-user'/>
                    <LeftMenuItem url={pageConstants.HELP} index={5} icon='pe-7s-help1' bottom={true}/>

                </div>

                
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

export default connect(
    mapStateToProps
)(LeftMenu);
