import React from 'react';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import * as leftMenuItemAction from './../../actions/menu/LeftMenuItemAction';
import '../../assets/z/styles/styles.scss';
import * as R from '../../resources/R';

class LeftMenuItem extends React.Component {

    constructor(props) {
        super(props);
        this.onClickHandler = this.onClickHandler.bind(this);
    }

    onClickHandler(){
        const {leftMenuItemAction, index, url} = this.props;
        browserHistory.replace(url);
        leftMenuItemAction.updateMenuIndex(index);
    }

    getIndex(){
        return this.state.menuIndex;
    }

    render(){
        const {icon, bottom, index, leftMenuItemReducer} = this.props;
        return  <div onClick={e=>this.onClickHandler()} style={bottom?{width:60, position:'absolute', bottom:0, height:50}:null} className={'z_leftMenuItem' + (index==leftMenuItemReducer.index?' z_active':'')}>
                    <div style={bottom?{marginTop:3}:null} className={icon+' z_i'}></div>
                </div>
    }
}

const mapStateToProps = (state) => {
    return {
        leftMenuItemReducer: state.leftMenuItemReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        leftMenuItemAction: bindActionCreators(leftMenuItemAction, dispatch),
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LeftMenuItem);
