import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import '../../assets/z/styles/styles.scss';
import * as R from '../../resources/R';
import LeftMenuItem from './LeftMenuItem';
import IconSweater from './../common/icon/IconSweater';
import IconBag from './../common/icon/IconBag';
import IconJean from './../common/icon/IconJean';
import IconKaki from './../common/icon/IconKaki';
import IconCulottes from './../common/icon/IconCulottes';
import IconShirt from './../common/icon/IconShirt';
import IconTShirt from './../common/icon/IconTShirt';
import IconSkirt from './../common/icon/IconSkirt';
import IconShorts from './../common/icon/IconShorts';
import IconCoat from './../common/icon/IconCoat';
import IconDress from './../common/icon/IconDress';
import IconGlasses from './../common/icon/IconGlasses';
import IconHat from './../common/icon/IconHat';

class LeftMenuProfile extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        return <div className='z_leftMenu'>
                   <div style={{marginLeft:40, marginTop:20, marginBottom:20}}>
                           <img style={{float:'left', borderRadius: '50%'}} width='40' height='40' src='https://lh3.googleusercontent.com/y-JQMbkvB0ukvApYggLpGdIGed6lJytBntWKOPT3CtO4h00I1Ab2doSYzt_giValaw=w300'/>
                           <div style={{float:'left', marginLeft:10}}>
                                <div style={{height:25, width:129, overflow:'hidden', fontSize:14, fontWeight:'bold', textOverflow: 'ellipsis', whitespace: 'nowrap'}}>
                                    Man Le
                                </div>
                                <div style={{marginTop:-5, height:22, width:129, overflow:'hidden', fontSize:12, textOverflow: 'ellipsis', whitespace: 'nowrap'}}>
                                    manle1389@gmail.coasdfasdf
                                </div>
                            </div>                       
                   </div>
                    <div style={{clear:'both', height:20}}></div>
                   <LeftMenuItem title='Tài khoản' icon='glyphicon glyphicon-user'></LeftMenuItem>
                   <LeftMenuItem title='Đơn hàng' icon='glyphicon glyphicon-shopping-cart'></LeftMenuItem>
                   <LeftMenuItem title='Đã xem' icon='glyphicon glyphicon-time'></LeftMenuItem>
                   <LeftMenuItem title='Sổ địa chỉ' icon='glyphicon glyphicon-book'></LeftMenuItem>
                </div>
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

export default connect(
    mapStateToProps
)(LeftMenuProfile);
