import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import '../../../assets/sg/styles/styles.scss';
class IconSkirt extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { color, width, height } = this.props;

        return (
            <svg x="0px" y="0px" viewBox="0 0 484.33 484.33" style={{marginLeft:-5, marginRight:-2}} width={width?width:'23'} height={height?height:'23'}>
                <path d="M371.665,122.781V57.962h-260v67.055L0,376.834l5.672,4.842c2.138,1.825,54.447,44.692,236.484,44.692  c182.045,0,234.362-42.867,236.501-44.692l5.673-4.842L371.665,122.781z M131.665,77.962h220v37.171h-220V77.962z M419.575,386.35  c-56.329,16.545-127.572,20.018-177.41,20.018c-49.846,0-121.088-3.473-177.414-20.017c-20.623-6.058-33.276-12.156-40.003-15.988  l104.31-235.23h46.058l-23.207,103.038l19.512,4.395l24.196-107.433h92.097l24.195,107.433l19.512-4.395l-23.206-103.038h47.049  l104.318,235.23C452.853,374.194,440.199,380.293,419.575,386.35z"></path>
            </svg>
        );
    }
}

export default IconSkirt;