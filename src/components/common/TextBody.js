import React, { Component } from 'react';
import PropTypes from 'prop-types';
import lodash from 'lodash';
import * as R from '../../resources/R';
import * as errorTypes from './../../constants/errorTypes';

class TextBody extends Component {
    constructor(props) {
        super(props);
        this.innerHTML = this.innerHTML.bind(this);
        this.renderPage = this.renderPage.bind(this);
    }

    innerHTML(){
        return this.textBody.innerHTML;
    }

    renderPage(){

        const {html, contentEditable} = this.props;
        let result = null;

        if(!contentEditable)
            result = 
                    <div ref={e=>this.textBody=e} id="popup-content" dangerouslySetInnerHTML={{__html: html }}></div>;
        else 
            result = 
                    <div style={{marginTop:10, border:'solid 1px #548516', borderRadius:10, padding:10, paddingTop:50}}>
                        <div style={{fontSize:11, padding:5, position:'absolute', backgroundColor:'#548516', color:'#fff', top:25, left:25}}>You can edit this form</div>
                        <div ref={e=>this.textBody=e} id="popup-content" contentEditable={this.props.contentEditable} dangerouslySetInnerHTML={{__html: html }}></div>
                    </div>
        return result;

    }

    render() {
        var {html} = this.props;
        if(!html){
            return <div className='sg_loading'></div>;
        }else if(html == errorTypes.ERROR_NO_CONTACT){
            return (<div style={{color:'#d9534f'}}>{R.UPDATE_CONTACT_EMAIL}</div>)
        }
        return this.renderPage();
    }
}

TextBody.propTypes = {
    html : PropTypes.string
};

export default TextBody;