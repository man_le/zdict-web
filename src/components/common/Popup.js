import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {Link} from 'react-router';
import * as pageConstants from './../../constants/pageConstants';
import { browserHistory } from 'react-router';
import lodash from 'lodash';
import * as R from '../../resources/R';
import '../../assets/sg/styles/styles.scss';
class Popup extends React.Component {

   constructor(props) {
       super(props);

        this.state = {
            open : false
        }
       this.renderCancelButton = this.renderCancelButton.bind(this);
       this.renderActionButtons = this.renderActionButtons.bind(this);
       this.close = this.close.bind(this);
       this.open = this.open.bind(this);
   }

   renderCancelButton(){
       const {buttonCancelId, hasCancel} = this.props;
       let result = null;
        if(hasCancel){
            result = <button onClick={this.close} id={buttonCancelId} className="button button--secondary button--green" data-dismiss="modal">{R.CANCEL}</button>
        }
        return result;
   }

   renderActionButtons(){
        const {id, buttons} = this.props;
        let result = [];

        let buttonType = 'button button--green';
        if(this.props.warning) buttonType = 'button button--yellow'

        {buttons.map((obj,i) => 
            result.push(
                <span key={i} style={{marginRight:'5px'}} key={i}>
                    <button 
                            disabled={obj.disabled?obj.disabled:''} 
                            onClick={(e)=>{lodash.isFunction(obj.onClick)?obj.onClick(e):null}} 
                            id={obj.id} 
                            className={buttonType}>{obj.text}
                    </button>
                </span>
            )
        )}
        return result;
   }
   
   close(){
        var {beforeClose} = this.props;
        if(beforeClose){
            beforeClose();
        }
        window.document.body.classList.remove('modal-open');
        this.setState({open:false});
   }

   open(){
        var {afterOpen} = this.props;
        
        if(afterOpen){
            afterOpen();
        }
        window.document.body.classList.add('modal-open');
        this.setState({open:true});
   }
  
   render() {
    const {id, title, children} = this.props;
    var {open} = this.state;
    if(!open){
        return null;
    }

    let modalStyle = 'modal-content sg_modal';
    if(this.props.warning) modalStyle = 'modal-content modal-dialog modal-dialog--small modal-dialog--warning sg_modal';

    return (
        <div>
            <div className="modal show" id={id} data-backdrop="static" data-keyboard="false">
                <div className='modal-dialog' style={this.props.width?{width:this.props.width}:null} role="document">
                    <div className={modalStyle} style={this.props.width?{width:this.props.width}:null}>
                    <div className="modal-header">
                        {
                            this.props.hideCloseMark
                            ?null
                            :<button onClick={this.close} type="button" className="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        }
                        <h5 className="modal-title">{title}</h5>
                    </div>
                    <div className="modal-body">
                        {children}
                    </div>
                    <div className="modal-footer sg_modal-footer">
                        {this.renderActionButtons()}
                        {this.renderCancelButton()}
                    </div>
                    </div>
                </div>
            </div>
            <div className="modal-backdrop in"></div>
        </div>
    );
   }
}

Popup.propTypes = {
    beforeClose: PropTypes.object,
    afterOpen: PropTypes.object,
    buttons: PropTypes.array,
    id: PropTypes.string,
    title: PropTypes.string,
    children : PropTypes.element.isRequired,
    hasCancel : PropTypes.bool,
    buttonCancelId: PropTypes.string
};

Popup.defaultProps = {
    buttons : []
}

export default Popup;