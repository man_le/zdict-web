import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';
import '../../assets/z/styles/styles.scss';
import * as R from '../../resources/R';

class ForgotPass extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        const {loginReducer} = this.props;
        if(loginReducer.userInfo || loginReducer.type=='CHECK_LOGIN') return null;
        return <div style={{fontSize:15, minWidth:884}}>
                   <div style={{height:50}}>
                       menu bar
                   </div>
                   <div style={{width:'80%', margin:'0 auto', marginTop:30}}>
                       <div className='col-md-6 col-sm-6'>adf</div>
                       <div className='z_boxshadow col-md-6 col-sm-6' style={{border:'solid 1px #ccc', height:260}}>
                            <div style={{textAlign:'center', marginTop:50}}>Enter your email</div>
                            <div style={{width:320, margin:'0 auto'}}>
                                <input className='dfi' type='text' style={{marginTop:10, width:320}} placeholder='Email'/>
                                <div style={{margin:'0 auto', marginTop:20, width:100}}>
                                    <button style={{width:100}} className='btn btn-primary'>Send</button>
                                </div>
                                <Link style={{textAlign:'center', display:'block', marginTop:5, fontSize:11}} to="/login">Login?</Link>
                            </div>
                       </div>
                   </div>
                </div>
    }
}

const mapStateToProps = (state) => {
    return {
        loginReducer: state.loginReducer
    };
};

export default connect(
    mapStateToProps
)(ForgotPass);
