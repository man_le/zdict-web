import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';
import '../../assets/z/styles/styles.scss';
import * as R from '../../resources/R';

class Login extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        const {loginReducer} = this.props;
        if(loginReducer.userInfo || loginReducer.type=='CHECK_LOGIN') return null;
        return <div style={{fontSize:15, minWidth:884}}>
                   <div style={{height:50}}>
                       menu bar
                   </div>
                   <div style={{width:'80%', margin:'0 auto', marginTop:30}}>
                       <div className='col-md-6 col-sm-6'>adf</div>
                       <div className='col-md-6 col-sm-6'>
                            <div className='z_boxshadow' style={{border:'solid 1px #ccc', height:360}}>
                                <div style={{textAlign:'center', marginTop:20}}>Login by</div>
                                <div style={{margin:'0 auto', marginTop:10, width:320}}>
                                    <a className='btn btn-danger' style={{width:150, fontWeight:'bold', marginRight:10}} href='http://localhost:3000/auth/google?re=http://localhost:4000'>Google</a>
                                    <a className='btn btn-primary blue' style={{width:150, fontWeight:'bold'}} href='http://localhost:3000/auth/facebook?re=http://localhost:4000'>Facebook</a>
                                </div>
                                <div style={{textAlign:'center', marginTop:50}}>Or enter your email and password</div>
                                <div style={{width:320, margin:'0 auto'}}>
                                    <input className='dfi' type='text' style={{marginTop:10, width:320}} placeholder='Email'/>
                                    <input className='dfi' type='password' style={{marginTop:10, width:320}} placeholder='Password'/>
                                    <div style={{margin:'0 auto', marginTop:20, width:100}}>
                                        <button style={{width:100}} className='btn btn-primary'>Dang nhap</button>
                                    </div>
                                    <Link style={{textAlign:'center', display:'block', marginTop:5, fontSize:11}} to='/forgotpass'>Forgot password?</Link>
                                </div>
                            </div>

                            <div className='z_boxshadow' style={{border:'solid 1px #ccc', height:250, marginTop:20}}>
                                <div style={{textAlign:'center', marginTop:50}}>Or create new account</div>
                                <div style={{width:320, margin:'0 auto'}}>
                                    <input className='dfi' type='text' style={{marginTop:10, width:320}} placeholder='Email'/>
                                    <input className='dfi' type='password' style={{marginTop:10, width:320}} placeholder='Password'/>
                                    <div style={{margin:'0 auto', marginTop:20, width:100}}>
                                        <button style={{width:100}} className='btn btn-primary'>Dang ky</button>
                                    </div>
                                </div>
                            </div>

                       </div>
                   </div>

                </div>
    }
}

const mapStateToProps = (state) => {
    return {
        loginReducer: state.loginReducer
    };
};

export default connect(
    mapStateToProps
)(Login);
