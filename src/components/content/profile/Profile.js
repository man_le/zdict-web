import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Textarea from "react-textarea-autosize";
import '../../../assets/z/styles/styles.scss';
import * as R from '../../../resources/R';

class Profile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            editIndex:-1
        }
        this.editIndexHandler = this.editIndexHandler.bind(this);
    }


    editIndexHandler(index){
        this.setState({
            editIndex : index
        })
        this.currentPassword.value = '';
        this.name.value = '';
        this.email.value = '';
    }

    render() {
        return <div className='z_profile'>

            <div className="row">
                <div style={{marginLeft:110, marginBottom:1, fontSize:20, fontWeight:'bold'}}>
                    Profile
                </div>
                <div style={{float:'none', margin:'0 auto', padding:'20px 0 20px 20px'}} className="col-md-10 col-sm-10 z_item z_boxshadow2">
                    <div className='z_inner'>
                        <div style={{marginBottom:20}}>
                            <div style={{float:'left'}}>
                                <img style={{borderRadius:'100%', width:80, height:80}} src="http://cdn2-www.dogtime.com/assets/uploads/2011/03/cute-dog-names.jpg"/>
                            </div>
                            <div style={{fontSize:30, marginLeft:90, paddingTop:10}}>Man Le</div>
                        </div>
                        
                        <table style={{fontSize:15, smarginTop:10, clear:'both', width:'100%'}}>
                            <tbody>
                            <tr height='40px'>
                                <td style={{fontWeight:'bold'}} width='100px'>Name</td>
                                <td width='400px'>
                                    <div className={(this.state.editIndex==0?'':'z_hidden') + ' z_editForm'}>
                                        <div className='z_l1'>
                                            <input ref={e=>this.name=e} placeholder='Name' style={{width:'100%'}} type='text' />
                                        </div>
                                        <div className='z_l2'>
                                            <div className='z_right'>
                                                <button className='btn btn-default' onClick={e=>this.editIndexHandler(-1)} style={{marginRight:10}}>Cancel</button>
                                                <button className='btn btn-primary'>Save</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={this.state.editIndex==0?'z_hidden':''}>Man Le</div>
                                </td>
                                <td width='50px'>
                                    <button onClick={e=>this.editIndexHandler(0)} className={'btn btn-primary '+(this.state.editIndex==0?'z_hidden':'')}>Edit</button>
                                </td>
                            </tr>
                            <tr height='40px'>
                                <td style={{fontWeight:'bold'}}>Email</td>
                                <td>
                                    <div className={(this.state.editIndex==1?'':'z_hidden') + ' z_editForm'}>
                                        <div className='z_l1'>
                                            <input ref={e=>this.email=e} placeholder='Email' style={{width:'100%'}} type='text' />
                                        </div>
                                        <div className='z_l1'>
                                            <input placeholder='New Password' style={{width:'100%'}} type='text' />
                                        </div>
                                        <div className='z_l2'>
                                            <div className='z_right'>
                                                <button className='btn btn-default' onClick={e=>this.editIndexHandler(-1)} style={{marginRight:10}}>Cancel</button>
                                                <button className='btn btn-primary'>Save</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={this.state.editIndex==1?'z_hidden':''}>manle1389@gmail.com</div>
                                </td>
                                <td>
                                    <button onClick={e=>this.editIndexHandler(1)} className={'btn btn-primary '+(this.state.editIndex==1?'z_hidden':'')}>Edit</button>
                                </td>
                            </tr>
                            <tr height='40px'>
                                <td style={{fontWeight:'bold'}}>Password</td>
                                <td>

                                    <div className={(this.state.editIndex==2?'':'z_hidden') + ' z_editForm'}>
                                        <div className='z_l1'>
                                            <input ref={e=>this.currentPassword=e} placeholder='Current Password' style={{width:'100%'}} type='text' />
                                        </div>
                                        <div className='z_l1'>
                                            <input placeholder='New Password' style={{width:'100%'}} type='text' />
                                        </div>
                                        <div className='z_l1'>
                                            <input placeholder='Confirm New Password' style={{width:'100%'}} type='text' />
                                        </div>
                                        <div className='z_l2'>
                                            <div className='z_right'>
                                                <button className='btn btn-default' onClick={e=>this.editIndexHandler(-1)} style={{marginRight:10}}>Cancel</button>
                                                <button className='btn btn-primary'>Save</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={this.state.editIndex==2?'z_hidden':''}>xxxxxxx</div>

                                </td>
                                <td>
                                    <button onClick={e=>this.editIndexHandler(2)} className={'btn btn-primary '+(this.state.editIndex==2?'z_hidden':'')}>Edit</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>

        </div>
    }
}

const mapStateToProps = (state) => {
    return {
        loginReducer: state.loginReducer
    };
};

export default connect(
    mapStateToProps
)(Profile);
