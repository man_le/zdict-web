import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import '../../../assets/z/styles/styles.scss';
import * as R from '../../../resources/R';

class ToolBox extends React.Component {

    constructor(props) {
        super(props);
        this.state={showToolBox:true};
        this.toolBoxHandler = this.toolBoxHandler.bind(this);
    }

    toolBoxHandler(e){
        if(this.state.showToolBox) this.setState({showToolBox:false});
        else this.setState({showToolBox:true});
    }

    render(){
        return <div style={{position:'fixed', width:50, right:30, bottom:20}}>
                   <button onClick={e=>this.toolBoxHandler(e)} style={{zIndex:9}} className='btn btn-primary z_btn-circle z_btn-lg'>
                       <div className={this.state.showToolBox?'pe-7s-close':'pe-7s-more'} style={{fontSize:30, marginTop:-1, marginLeft:-6}}></div>
                   </button>

                   <div className={this.state.showToolBox?null:'z_hidden'}>
                        <div style={{borderRadius:20, position:'absolute', marginTop:-52, marginLeft:-320, width:350, height:55, backgroundColor:'#323439'}}>
                            <div style={{backgroundColor:'#fff', width:55, height:55, borderRadius:50, marginLeft:317}}></div>
                        </div>

                        <div style={{position:'absolute', marginTop:-40, marginLeft:-310, color:'#fff'}}>
                            <div style={{marginLeft:30}} className='z_left z_bottomMenuItem'>
                                <span className='pe-7s-search' style={{fontSize:30}}></span>
                                <span>En</span>
                            </div>

                            <div className='z_left z_bottomMenuItem'>
                                <span className='pe-7s-search' style={{fontSize:30}}></span>
                                <span>Vi</span>
                            </div>

                            <div className='z_left' style={{marginLeft:10, marginRight:10, borderLeft:'1px solid #595959', borderRight:'1px solid #1a1a1a', height:30}}></div>

                            <div className='z_left z_bottomMenuItem' style={{paddingLeft:15}}>
                                <span className='pe-7s-refresh-2' style={{fontSize:30}}></span>
                            </div>

                            <div className='z_left z_bottomMenuItem' style={{paddingLeft:13}}>
                                <span className='pe-7s-diskette' style={{fontSize:30}}></span>
                            </div>
                        </div>
                   </div>
                   

                </div>
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

export default connect(
    mapStateToProps
)(ToolBox);
