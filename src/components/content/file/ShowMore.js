import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import '../../../assets/z/styles/styles.scss';
import * as R from '../../../resources/R';

class ShowMore extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        return <div className={(this.props.showMore?'':'z_hidden') +' z_file_more'}>
        <div className='z_inner'>
            <div className='z_showMoreGroup'>
                <div className='pe-7s-close z_clearSearch'></div>
                <input className='form-control dfi z_smsearch' autoComplete="off" autoCorrect="off" autoCapitalize="off" spellCheck='false' type='text' placeholder='Search your phrases...'/>
            </div>
            <div className='z_grr'>

                <div className='z_l1'>
                    Cau goc
                </div>
                <div className='z_lg'>
                    <div className='z_left z_lg1'>
                        <span className='z_voca'>Look</span>
                        <div className='z_type'>Verb</div>
                    </div>
                    <div className='z_left z_lg1'>
                        <span className='z_voca'>Look</span>
                        <div className='z_type'>Verb</div>
                    </div>
                    <div className='z_left z_lg1'>
                        <span className='z_voca'>Look</span>
                        <div className='z_type'>Verb</div>
                    </div>
                </div>
                <div className='z_right z_grouptoolmini'>
                    <div>
                        <div className='pe-7s-volume z_left z_item'></div>
                        <div className='pe-7s-copy-file z_left z_item'></div>
                        <div className='pe-7s-like z_left z_item'></div>
                    </div>
                </div>

                <div className='z_l1 z_lgn'>
                    Chuyen thanh ...
                </div>
                <div>
                    <div className="btn-group z_btnGroup">
                        <button type="button" className="btn btn-default dropdown-toggle z_btnDD" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Cau phu dinh <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu z_ulDD">
                            <li><a href="#">Cau phu dinh</a></li>
                            <li role="separator" className="divider"></li>
                            <li><a href="#">Danh tu so it</a></li>
                            <li><a href="#">Danh tu so nhieu</a></li>
                            <li role="separator" className="divider"></li>
                            <li><a href="#">So thanh chu so</a></li>
                            <li><a href="#">Chu so thanh so</a></li>
                            <li role="separator" className="divider"></li>
                            <li><a href="#">Lam ro nghia</a></li>
                            <li role="separator" className="divider"></li>
                            <li><a href="#">Chuyen sang thi qua khu don</a></li>
                        </ul>
                    </div>
                </div>

                <div className='z_l1 z_lgn'>
                    Cau phu dinh
                </div>
                <div className='z_lg'>
                    <div className='z_left z_lg1'>
                        <span className='z_voca'>Look</span>
                        <div className='z_type'>Verb</div>
                    </div>
                    <div className='z_left z_lg1'>
                        <span className='z_voca'>Look</span>
                        <div className='z_type'>Verb</div>
                    </div>
                    <div className='z_left z_lg1'>
                        <span className='z_voca'>Look</span>
                        <div className='z_type'>Verb</div>
                    </div>
                </div>
                <div className='z_right z_grouptoolmini'>
                    <div>
                        <div className='pe-7s-volume z_left z_item'></div>
                        <div className='pe-7s-copy-file z_left z_item'></div>
                        <div className='pe-7s-like z_left z_item'></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

export default connect(
    mapStateToProps
)(ShowMore);
