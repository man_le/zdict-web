import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Textarea from "react-textarea-autosize";
import '../../../assets/z/styles/styles.scss';
import * as R from '../../../resources/R';

class Folder extends React.Component {

    constructor(props) {
        super(props);
    }
   

    render() {
        return <div className='z_folder'>
            
            <div style={{margin:'0 auto', width:'60%', height:35}}>
                <input style={{marginLeft:100}} placeholder='Search...' type='text' className='form-control'/>
                <div style={{marginTop:-58}} className="btn-group">
                    <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action <span className="caret"></span>
                    </button>
                    <ul className="dropdown-menu">
                        <li><a href="#">Lịch</a></li>
                        <li><a href="#">Text</a></li>
                    </ul>
                </div>
            </div>

            <div className="row z_group">
                <div className="col-md-3 col-sm-3 z_item">
                    <div className='z_boxshadow z_inner'>
                        <div className='z_title'>
                            this is title
                        </div>
                        <div className='z_content'>
                            Hang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thu
                        </div>
                        <div className='z_bottom'>ss</div>    
                    </div>
                </div>
                <div className="col-md-3 col-sm-3 z_item">
                    <div className='z_boxshadow z_inner'>
                        <div className='z_title'>
                            this is title
                        </div>
                        <div className='z_content'>
                            Hang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thu
                        </div>
                        <div className='z_bottom'>ss</div>    
                    </div>
                </div>
                <div className="col-md-3 col-sm-3 z_item">
                    <div className='z_boxshadow z_inner'>
                        <div className='z_title'>
                            this is title
                        </div>
                        <div className='z_content'>
                            Hang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thu
                        </div>
                        <div className='z_bottom'>ss</div>    
                    </div>
                </div>
                <div className="col-md-3 col-sm-3 z_item">
                    <div className='z_boxshadow z_inner'>
                        <div className='z_title'>
                            this is title
                        </div>
                        <div className='z_content'>
                            Hang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thuHang nam cu vao cuoi thu
                        </div>
                        <div className='z_bottom'>ss</div>    
                    </div>
                </div>
            </div>

        </div>
    }
}

const mapStateToProps = (state) => {
    return {
        loginReducer: state.loginReducer
    };
};

export default connect(
    mapStateToProps
)(Folder);
