import React from 'react';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Textarea from "react-textarea-autosize";
import '../../../assets/z/styles/styles.scss';
import * as R from '../../../resources/R';

class Favorite extends React.Component {

    constructor(props) {
        super(props);
    }
   
    render() {

        return(
        <div>
        
        <div className='z_history'>
            <div style={{zIndex:8}} className='z_grs z_boxshadow'>
                <div style={{width:'70%', marginLeft:90}}>
                    <div className='pe-7s-close z_clearSearch'></div>
                    <input className='form-control z_fsearch dfi' autoComplete="off" autoCorrect="off" autoCapitalize="off" spellCheck='false' type='text' placeholder='Search your history...'/>
                </div>
            </div>
            
            <div className='z_grresult'>

                <div style={{marginTop:20}} className=' z_text'>
                    <div>
                        ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
                    </div>

                    <div className='z_tool'>
                        <div className='pe-7s-volume z_left z_fitem z_i1'></div>
                        <div className='pe-7s-copy-file z_left z_fitem z_i1'></div>
                        <div className='pe-7s-search z_left z_fitem z_i2'></div>
                        <div className='pe-7s-trash z_right z_fitem z_i3'></div>
                    </div>
                </div>
                
                <div style={{marginTop:20}} className=' z_text'>
                    <div>
                        ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
                    </div>

                    <div className='z_tool'>
                        <div className='pe-7s-volume z_left z_fitem z_i1'></div>
                        <div className='pe-7s-copy-file z_left z_fitem z_i1'></div>
                        <div className='pe-7s-like z_left z_fitem z_i2'></div>
                        <div className='pe-7s-trash z_right z_fitem z_i3'></div>
                    </div>
                </div>
            </div>
        </div>

        </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginReducer: state.loginReducer
    };
};

export default connect(
    mapStateToProps
)(Favorite);
