import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Textarea from "react-textarea-autosize";
import '../../assets/z/styles/styles.scss';
import * as R from '../../resources/R';

class Content extends React.Component {

    constructor(props) {
        super(props);
    }
   

    render() {
        return <div>
                    {this.props.children}
                </div>
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

export default connect(
    mapStateToProps
)(Content);
