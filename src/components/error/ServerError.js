import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import '../../assets/sg/styles/styles.scss';
import * as R from '../../resources/R';

const ServerError = () => {
    return (
        <div className='sg_errorPage'>
            <div className='sg_group' style={{width:'380px'}}>
                <div style={{height:'10px'}}></div>
                <div>
                    <div className='sg_title sg_absolute_icon sg_center'></div>
                    <div style={{color:'#777', fontWeight:'bold', fontSize:'20px'}} className='sg_contentCenter'>
                        <span id="error-serverError-content">{R.SOMTHING_WRONG}</span>
                        <div style={{width:'150px', margin:'auto', marginTop:'20px'}}>
                            <p style={{marginTop:'7px', fontSize:'12px', fontWeight:'none', color:'#ccc'}}>
                                <a id="error-serverError-back" href='/'>{R.GO_BACK}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default ServerError;
