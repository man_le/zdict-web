import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import '../../assets/sg/styles/styles.scss';
import * as R from '../../resources/R';

const Error500 = () => {
    return (
        <div style={{marginTop:'30px'}}>
            <div style={{borderRadius:'10px', height:'90px', width:'270px', backgroundColor:'#d9d9d9', margin:'auto'}}>
                <div style={{height:'10px'}}></div>
                <div>
                    <div  style={{float:'left', height:'40px'}} className='sg_absolute_icon sg_center'></div>
                    <div style={{color:'#777', fontWeight:'bold', fontSize:'20px'}}>
                        <span id="error-500-content">{R.INTERNAL_SERVER_ERROR}</span>
                        <div style={{width:'150px', margin:'auto', marginTop:'20px'}}>
                            <p style={{textAlign:'center', marginTop:'7px', fontSize:'12px', fontWeight:'none', color:'#ccc'}}>
                                <a id="error-500-back" href='/'>{R.GO_BACK}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Error500;
