import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import * as R from '../../resources/R';
import '../../assets/sg/styles/styles.scss';

const Error404 = () => {
    return (
        <div className='sg_errorPage'>
            <div className='sg_group'>
                <div style={{height:'10px'}}></div>
                <div>
                    <div className='sg_title sg_absolute_icon sg_center'></div>
                    <div className='sg_content'>
                        <span id="error-404-content">{R.PAGE_NOT_FOUND}</span>
                        <div className='sg_bottom'>
                            <p className='sg_back'>
                                <a id="error-404-back" href='/'>{R.GO_BACK}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Error404;
