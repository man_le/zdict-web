export const VIEW_ORDER = 'Theo dõi đơn hàng';
export const VIEW_ACCOUNT = 'Thông tin tài khoản';
export const PRODUCT_HISTORY = 'Sản phẩm đã xem';
export const ADDRESS_BOOK = 'Sổ địa chỉ';
export const LOGOUT = 'Thoát';