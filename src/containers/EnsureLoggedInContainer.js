import React from 'react';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import {checkLogin} from '../actions/login/LoginAction';
import * as leftMenuItemAction from './../actions/menu/LeftMenuItemAction';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import * as pageConstants from './../constants/pageConstants';

class EnsureLoggedInContainer extends React.Component {

constructor(props) {
  super(props);
  this.leftMenuHandler = this.leftMenuHandler.bind(this);
}

componentWillMount () {
  this.props.checkLogin(this.props.currentURL);
  this.leftMenuHandler();

}

leftMenuHandler(){
  const {leftMenuItemAction, currentURL} = this.props;
  if(currentURL==pageConstants.ROOT) leftMenuItemAction.updateMenuIndex(1);
  else if(currentURL==pageConstants.HISTORY) leftMenuItemAction.updateMenuIndex(2);
  else if(currentURL==pageConstants.FAVORITE) leftMenuItemAction.updateMenuIndex(3);
  else if(currentURL==pageConstants.PROFILE) leftMenuItemAction.updateMenuIndex(4);
}

render() {
    return this.props.children;
  }
}

function mapStateToProps(state, ownProps) {
  return {
    currentURL: ownProps.location.pathname,
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        checkLogin: (url) => dispatch(checkLogin(url)),
        leftMenuItemAction: bindActionCreators(leftMenuItemAction, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EnsureLoggedInContainer)