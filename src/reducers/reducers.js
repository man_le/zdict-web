import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import * as types from '../actions/types';
import * as baseAction from '../actions/base';
import leftMenuItemReducer from './menu/LeftMenuItemReducer';
import loginReducer from './login/loginReducer';

const rootReducer = combineReducers({
    routing,
    leftMenuItemReducer,
    loginReducer
});

export default rootReducer;