import * as types from '../../actions/types';
import * as obj from '../../actions/menu/LeftMenuItemAction';

const LeftMenuItemReducer = ( state = obj.getState(), action) => {

    switch (action.type) {
        case types.UPDATE_MENU_INDEX:
        case types.UPDATE_MENU_INDEX_SUCCESS:
            return action;
            
        default:
            return state;
    }
};

export default LeftMenuItemReducer;
