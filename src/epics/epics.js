import { combineEpics } from 'redux-observable';

import {checkLoginEpic, checkLoginEpicFail, loginEpic} from './login/loginEpic';

const rootEpic = combineEpics(
    checkLoginEpic, checkLoginEpicFail, loginEpic
);

export default rootEpic;