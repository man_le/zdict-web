Service Guarantee UI
===========
**Framework :**
- ReactJS, Redux, Redux-Observable, Webpack

**Command  :**
- Install : npm install
- Build : npm run build
- Run for development : npm start

**Notes :**
- variable.js : config for develop environment
- check systact : https://babeljs.io/
- Css : https://bootswatch.com/


