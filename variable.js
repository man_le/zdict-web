
const HOST_NAME = process.env.HOST || "localhost";
const HOST = process.env.HOST || "http://localhost";
const PORT = process.env.PORT || "4000";

const PROXY_DEV = 'http://localhost:3000';

module.exports = {
    host_name: HOST_NAME,
    host: HOST,
    port: PORT,
    proxy_dev: PROXY_DEV
};
